# **DevTube**

**YouTube** client *Tailored*  for the Developers

- No useless recommendations
- No tracking of data
- Important searches in Side-panel
- User able to search for videos and watch
- Amazing channel profile
- In video detail page user can control playing like real youtube

## Technologies:

- React Js
- Material Ui 5
- Axios
- React router dom
- Rapid Api(youtube v3 API)

### live: https://dev-tube.vercel.app/

[![DevTube](./public/seo.png)](https://dev-tube.vercel.app/)